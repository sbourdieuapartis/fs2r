# 1. Summary

## 1.1. Brief description of the described research output

What kind of research output are you describing?
Research data is information (particularly facts or numbers) collected to be examined and considered, and to serve as a basis for reasoning, discussion or calculation. Open access to research data - the right to access and reuse digital research data under the terms and conditions set out in the Grant Agreement.
<br>Other research outputs refer to outputs that are produced or reused during the research and data management lifecycle. They might be objects, instruments and materials in digital form, such as software, workflows, protocols, models, etc. Equally, in their analog form they might include new materials, antibodies, reagents, samples, etc.

## 1.2. Is it physical or digital?

Are you generating or re-using it?
Apart from data produced within your research, you might want to repurpose data that have been produced and shared by others, in different research context.If you are planning to use other researchers data in your research there are a couple of things that you need to take into consideration, such as copyright of datasets, permissions provided through licenses, ethical aspects for reuse, etc.If you are unsure of the license of a dataset that you are re-using, you may use the <a href="https://lct.ni4os.eu/lct/login" target="_blank">License Clearance Tool</a>.
<br>Learn more:<a href="https://www.openaire.eu/can-i-reuse-someone-else-research-data" target="_blank">https://www.openaire.eu/can-i-reuse-someone-else-research-data</a>.

## 1.3. What is the type of the described dataset?
The main distinction between data types is between primary data and secondary data. Data collection may contain both primary and secondary data depending on the source where they have been derived from.Primary data is data that have been collected for the first time and have not undergone through data processing and/or analysis, yet.Secondary data is data that have been cleaned up, analysed and shared by others (published or unpublished) and they are those that are being typically reused.In addition, types of data unveil how they have been collected so they also depend on the process that led to their generation. 
<br>For more information, have a look at:<a href="https://data.library.virginia.edu/data-management/plan/format-types/" target="_blank">https://data.library.virginia.edu/data-management/plan/format-types/</a>   

## 1.4. What is its format?
Different types of data are acquired, processed and stored (preserved and/or archived) in different ways and can be discipline specific. Many proprietary file formats are containers for standard file formats. By packaging them into these containers, a software and/or hardware developer can provide additional functionality, usually by streamlining a process, to analyse data acquired on their platform. However, this has the negative consequence of making these data less interoperable.
<br>You may find some best practices for file formats:<a href="https://library.stanford.edu/research/data-management-services/data-best-practices/best-practices-file-formats" target="_blank">https://library.stanford.edu/research/data-management-services/data-best-practices/best-practices-file-formats</a>
<br>For more information about preserving data in appropriate formats:<a href="https://www.openaire.eu/data-formats-preservation-guide" target="_blank">https://www.openaire.eu/data-formats-preservation-guide</a>

## 1.5. What is its expected size?
Give measurable examples per type of output based on common practices in the field. These might be relevant to the volumes of data and how many bytes of storage they occupy, numbers of objects, files, rows, and columns.

## 1.6. Why are you collecting/generating or re-using it?
Data collection is usually at the beginning stages of research data management lifecycles to set the background of what is needed (data generation), what is already there (data reuse) and how to best use it to fulfill the projectâ€™s objectives (why).

## 1.7. What is its origin / provenance?

## 1.8. To whom might it be useful ('data utility')?

<br>

# 2.	Link between outputs [Publications / Datasets / Software]

## 2.1. Does the described output support any scientific publication? [Publications]
Create the links between scientific publications and research data.

## 2.2. Is there a data availability statement provided along with the publication? [Publications]

## 2.3. Does the described output use or support any published dataset? [Datasets]

## 2.4. Does the described output use or support any software? [Software]

<br>

# 3.	Fair practices [Findability / Repository / Data / Embargo / Methods / Interoperability / Licensing]

## 3.1. Making data and other outputs findable, including provisions for metadata
In general terms, your research data should be 'FAIR', that is findable, accessible, interoperable and re-usable. These principles precede implementation choices and do not necessarily suggest any specific technology, standard, or implementation-solution. Achieving FAIR data relies equally on the choices you make throughout a research process, but also on providers whose services enable FAIRness of data to be met. 
<br>Learn more about the FAIR principles: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4792175/ 
<br>Learn how you can comply with FAIR principles: https://www.openaire.eu/how-to-make-your-data-fair
Data are findable when described with metadata and vocabularies in a standardized way, assigned Persistent Identifiers (PIDs) and are registered or indexed in a searchable resource.

## 3.2. What type(s) of persistent identifier(s) are used for the described dataset / output? [Findability]
Persistent and/or Permanent Identifiers (PIDs) uniquely identify objects, people, organisations and activities and can ensure that the scientific output is accessible even when the URL of the website has changed. PIDs can be assigned to research outputs including publications, data and software/code. PIDs can also be assigned to researchers, samples, organisations and projects. A PID may be connected to a metadata record describing an item rather than the item itself. PIDs are usually provided by data repositories and other deposit platforms. Re3data includes tags to show which platforms that it indexes assign PIDs to their content.

## 3.3. Will you provide metadata for the described dataset / output? [Findability]
Metadata is data about data and is an essential set of information describing scientific outputs, in the form of either physical or digital objects, in a machine-readable format. According to the expected use, metadata can be given different attributes. Most common type which enables discovery and identification are descriptive metadata. Descriptive metadata contain information about key aspects needed to search for and successfully find a given scientific output, e.g. by its title, author/creator, abstract, keywords. Moreover, metadata may be used for describing a service or a scientific instrument.

## 3.4. Making data and other outputs openly accessible [Findability]
Not all data can be made publicly open, hence data can be FAIR but not open, or open but not FAIR or both FAIR and open. Data are accessible when uploaded in a data repository and retrieved by their PIDs. When data can not be shared openly, metadata should be provided (even when the data are no longer available). In the case of sensitive or personal data, anonymization or pseudonymization and specific access rights can be applied. Where accessing data requires the use of complementary methods or tools, such procedures should be documented.

## 3.5. In which repository will the dataset / output be deposited? [Repository]
A data repository, otherwise known as a data archive, is an online collection of datasets that are described and classified in a standard way that makes data discovery and retrieval easier to be performed by both humans and machines. 
<br>For more information on the processes for datasets, you can have a look at https://www.openaire.eu/rdm-handbook

## 3.6. Is the selected repository a trusted source? [Repository]
Trusted repositories assume a central role in the Horizon Europe for the deposition of and access to publications and research data. They are: 
* Certified repositories (e.g. CoreTrustSeal, nestor Seal DIN31644, ISO16363) or disciplinary and domain repositories commonly used and endorsed by the research communities. Such repositories should be recognised internationally.
* General-purpose repositories or institutional repositories that present the essential characteristics of trusted repositories, i.e.: 
    * display specific characteristics of organisational, technical and procedural quality such as services, mechanisms and/or provisions that are intended to secure the integrity and authenticity of their contents, thus facilitating their use and re-use in the short- and long-term. Trusted repositories have specific provisions in place and offer explicit information online about their policies, which define their services (e.g. acquisition, access, security of content, long-term sustainability of service including funding etc.).
    * provide broad, equitable and ideally open access to content free at the point of use, as appropriate, and respect applicable legal and ethical limitations. They assign persistent unique identifiers to contents (e.g. DOIs, handles, etc.), such that the contents (publications, data and other research outputs) are unequivocally referenced and thus citeable. They ensure that contents are accompanied by metadata sufficiently detailed and of sufficiently high quality to enable discovery, reuse and citation and contain information about provenance and licensing; metadata are machine- actionable and standardized (e.g. Dublin Core, Data Cite etc.) preferably using common non-proprietary formats and following the standards of the respective community the repository serves, where applicable.
    * facilitate mid- and long-term preservation of the deposited material. They have mechanisms or provisions for expert curation and quality assurance for the accuracy and integrity of datasets and metadata, as well as procedures to liaise with depositors where issues are detected. They meet generally accepted international and national criteria for security to prevent unauthorized access and release of content and have different levels of security depending on the sensitivity of the data being deposited to maintain privacy and confidentiality.

## 3.7. Add appropriate arrangements made with the repository(ies) where the described dataset will be deposited 

## 3.8. Does the repository(ies) assign datasets / outputs with persistent identifiers? 
Persistent identifiers (PIDs) are key in ensuring the findability of research outputs, including data. They are globally unique and long-lasting references to digital objects (such as data, publications and other research outputs) or non-digital objects such as researchers, research institutions, grants, etc. Frequently used persistent identifiers include digital object identifiers (DOIs), Handles, and others. 
<br>For further reading on PID types, please refer to https://www.dpconline.org/handbook/technical-solutions-andtools/persistent-identifiers.

## 3.9. Does the repository support versioning? 

## 3.10. What is the described dataset / output title? [data]

## 3.11. How is the dataset / output shared? [data]
Indicates access mode for data. If certain datasets cannot be shared (or need to be shared under restricted access conditions), explain why, clearly separating legal and contractual reasons from intentional restrictions. Note that in multi-beneficiary projects it is also possible for specific beneficiaries to keep their data closed if opening their data goes against their legitimate interests or other constraints as per the Grant Agreement.

## 3.12. What is the reason of limiting access to the dataset / output? [embargo]
Explain why the described dataset or parts of its distribution cannot be shared (or need to be shared under restricted access conditions),clearly separating legal and contractual reasons from intentional restrictions. Note that in multi-beneficiary projects it is also possible for specific beneficiaries to keep their data closed if opening their data goes against their legitimate interests or other constraints as per the Grant Agreement.
Embargo periods are tied to sharing and publishing scientific outputs. An embargo might be applied to give time to publish or seek protection of the intellectual property (e.g. patents). If an embargo is applied to give time to publish or seek protection of the intellectual property (e.g. patents), specify why and how long this will apply, bearing in mind that research data should be made available as soon as possible.

## 3.13. If an embargo applies, please specify when the dataset / output will be made available [embargo]
If an embargo is applied to give time to publish or seek protection of the intellectual property (e.g. patents), specify why and how long this will apply, bearing in mind that research data should be made available as soon as possible.

## 3.14. Are there any methods or tools required to access the dataset / output? [methods]
Describe processes needed to query and access data. These might be relevant to SPARQL access points, visualizer software, download, etc. Data access controls, such as passwords or firewalls, should be used to limit access to confidential data and protect them from unauthorised changes.

## 3.15. Please provide information about the method(s) needed to access the dataset / output. 

## 3.16. Please provide information about the tools needed to access the dataset / output. 

## 3.17. Is the described dataset / output supported by a data access committee? 
Explain if there is a need for a dedicated data access committee to evaluate/approve access requests to personal/sensitive data, etc.

## 3.18. Please specify how the dataset / output will be accessed during and after the project ends 

## 3.19. Please specify how long after the project has ended the dataset / output will be made accessible for 
Please check with funders' or institutions's policies which might apply an embargo on data access.

## 3.20. Will you provide metadata even if the described dataset / output can not be openly shared? [metadata]

## 3.21. Under which license will metadata be provided? [licensing]
According to the Grant Agreement, metadata are made openly available and licensed under apublic domain dedication CC0.

## 3.22. Do metadata provide information about how to access the described dataset / output? [metadata]

## 3.23. Will metadata remain available after the dataset / output is no longer available? [metadata]

## 3.24. Does your (meta)data use a controlled vocabulary? [Interoperability]
Controlled vocabularies provide standard terminology as opposed to keywords or tags used to classify information. Examples: taxonomies, ontologies, thesauri.

## 3.25. If you created the vocabulary, where can it be found? [Interoperability]	
Consider openly publishing the generated ontologies or vocabularies to allow reusing, refining or extending them.

## 3.26. Have you applied a standard schema for your (meta)data? [Interoperability]
Description: You may browse the Metadata Standards Catalog here:<a href="http://rd-alliance.github.io/metadata-directory/" target="_blank">http://rd-alliance.github.io/metadata-directory/</a>

## 3.27. Will you provide a mapping to more commonly used ontologies? [Interoperability]

## 3.28. What is the methodology followed? [Interoperability]

## 3.29. What community-endorsed interoperability best practices are followed? [Interoperability]

## 3.30. Does the described dataset / output provide qualified references with other outputs? [Interoperability]
A qualified reference is a cross-reference that explains its intent. For example, X is regulator of Y is a much more qualified reference than X is associated with Y, or X see also Y. The goal therefore is to create as many meaningful links as possible between (meta)data resources to enrich the contextual knowledge about the data.<a href="https://www.go-fair.org/fair-principles/i3-metadata-include-qualified-references-metadata/"></a>

## 3.31. What internationally recognised licence will you use for your dataset / output? [licensing]
There are a number of licenses that can be assigned to your research outputs. There are different licenses for software and for publications and data. For the latter, a common practice is the use of Creative Commons licenses which are machine readable.For more information on the licences required for data under Horizon Europe, please refer to the AGA (article 17).
<br> You may learn more about how to choose a license here:<a href="https://www.openaire.eu/how-do-i-license-my-research-data" target="_blank">https://www.openaire.eu/how-do-i-license-my-research-data</a>
<br> You may use the License Clearance Tool (LCT) to check compliance of licenses for derivative works:<a href="https://lct.ni4os.eu/" target="_blank">https://lct.ni4os.eu/</a>

## 3.32. What reusability and / or reproducibility methods are followed?
Provide supplementary material that validate data analysis and facilitate data re-use.

## 3.33. Will you provide the described dataset / output in the public domain? [licensing]

## 3.34. Do you intend to ensure (re)use by third parties after your project finishes?
Despite where data are stored for everyday activities during the project lifetime, there are certain options to choose from data storage that ensures reuse in the log run. Most popular are data archives, otherwise found as repositories. Depending of data volume, archiving might be subject to small fees.

## 3.35. Is provenance well documented?
Data provenance, or data lineage, is a type of metadata that tracks digital object's history in time and its relations with other entities. You may also check:<a href="https://casrai.org/term/provenance-metadata/" target="_blank">https://casrai.org/term/provenance-metadata/</a>

## 3.36. What documented procedures for quality assurance do you have in place?
Quality assurance procedures are closely related to RDM and are meant to validate that data are clean (no duplicates or inconsistencies), error-free, well structured and represented in analysis. Quality assurance checks can be facilitated by the use of tools or scripts.To familiarise yourself with the process, you may find a checklist here:<a href="https://www.eur.nl/sites/corporate/files/2017-11/Checklist_QA_QC_RDM_0_2_MD_20150129.pdf" target="_blank">https://www.eur.nl/sites/corporate/files/2017-11/Checklist_QA_QC_RDM_0_2_MD_20150129.pdf</a>

<br>

# 4. Allocation of resources

## 4.1. What will be the cost of making the described output FAIR?
To understand what costs are allocated across the steps of a RDM lifecycle, how to estimate them and how to prevent them, please check:<a href="https://www.openaire.eu/how-to-comply-to-h2020-mandates-rdm-costs" target="_blank">https://www.openaire.eu/how-to-comply-to-h2020-mandates-rdm-costs</a>

## 4.2. How will this cost be covered?
Note that costs related to research data/output management are eligible as part of the Horizon Europe grant (if compliant with the Grant Agreement conditions).

## 4.3. Identify the people who will be responsible and their role(s) in the management of the described output
Provide names and responsibilities of researchers or data managers data management and stewardship activities that are performed throughout the project for the described output. Follow the research data management and software management lifecycles to guide your steps in data capture, software development,, metadata production, data qualitystorage and backup, data and software archiving and sharing

<br>

# 5. Security

## 5.1. What security measures are followed?
Describe what provisions are in place for data security.

## 5.2. What conditions do the security measures meet?
Describe data provisions, including data recovery as well as secure storage/archiving and transfer of sensitive data.

## 5.3. How will you preserve the described dataset / output in the long term?
Please describe curation and preservation policies followed for repository content.

<br>

# 6. Ethical aspects

## 6.1. Are there any ethical or legal issues that can have an impact on sharing the described dataset / output?

## 6.2. Does the described dataset / output contain sensitive information?

## 6.3. Does the described dataset / output contain personal data?

<br>

# 7. Other

## 7.1. Do you make use of other procedures for data management?
Please check:<a href="https://www.scienceeurope.org/our-resources/guidance-document-presenting-a-framework-for-discipline-specific-research-data-management" target="_blank">https://www.scienceeurope.org/our-resources/guidance-document-presenting-a-framework-for-discipline-specific-research-data-management</a>,
<br><a href="https://www.dtls.nl/fair-data/research-data-management/data-management-knowledge-tools/" target="_blank">https://www.dtls.nl/fair-data/research-data-management/data-management-knowledge-tools/</a>  

## 7.2. Documentation of other procedures 

